﻿---
title: First adventure
subtitle: Teamcaptain
date: 2017-09-04
---

DESIGN CHALLENGE 1 /
Vandaag was de eerste dag van de challenge, we hebben inhoudelijke informatie gekregen over de deliverables en hebben daar ook al aan gewerkt. Ik ben ook vanaf vandaag de teamcaptain.

Voordat we aan de deliverables gewerkt hebben moesten we eerst een teamposter maken. We hebben eerst een paar ontwerpen gemaakt waarna we de beste hebben uitgekozen en deze hebben uitgewerkt. Hier op hebben we onze kwaliteiten, achtergronden, ambities en regels geschreven.

Ik heb als teamcaptain gezorgd dat we een paar regels hebben vastgesteld waar we ons als team gedurende het kwartaal aan gaan houden.
De wensen van het team over onze studio heb ik ook verwoord bij de vergadering met de teamcaptains van de andere groepen.
Hiernaast  heb ik vandaag als teamcaptain geprobeerd de taken en deliverables op een rij te zetten zodat deze later verdeeld kunnen worden.

Ik heb ook de planning gemaakt voor de aankomende twee weken zodat de taken en de verdeling hiervan overzichtelijk worden. Het verzinnen van onderzoeksvragen voor onze doelgroep is ook begonnen. 


Oh... we hebben vandaag ook onderzoek gedaan naar verschillende studies op de hro en hiervan hebben we er een uitgekozen.
